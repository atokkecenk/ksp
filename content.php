<?php
if ($_GET["page"] == 'logout') {
    session_destroy();
    echo "<meta http-equiv='refresh'content='0;url=index.php'> ";
} else {
    $f = 'modul/';
    $page = $_GET["page"];
    $file = $f . "$page.php";
    if (!file_exists($file) || empty($file)) {
        include $f . "404.php";
    } else {
        include $file;
    }
}
