<?php
$aks = isset($_GET['aks']) ? $_GET['aks'] : '';
$id = isset($_GET['id']) ? $_GET['id'] : '';
$ie = isset($_GET['ie']) ? $_GET['ie'] : '';
$msg = '';
if ($aks == 'upd' && $ie !== '') {
    $edit = mysql_fetch_array(mysql_query("SELECT * FROM tb_rek3 WHERE idrek3 = '$ie'"));
}
if (isset($_POST['simpan'])) {
    $idrek2 = $_POST['idrek2'];
    $idrek3 = $_POST['idrek3'];
    $kd = $_POST['kd_rek'];
    $nm = $_POST['nama_rek'];
    $na = isset($_POST['na']) ? 'Y' : 'N';
    if ($idrek3 == '') {
        $qry = mysql_query("INSERT INTO tb_rek3 (idrek2, koderek3, namarek3, NA) VALUES (
            '$idrek2',
            '$kd',
            '$nm',
            '$na'
            )");

        $text_Y = 'Berhasil simpan rekening.';
        $text_N = 'Gagal simpan rekening.';
    } else {
        $qry = mysql_query("UPDATE tb_rek3 SET
            koderek3 = '$kd',
            namarek3 = '$nm',
            NA = '$na' 
            WHERE idrek3 = '$idrek3'
            ");

        $text_Y = 'Berhasil perbarui rekening.';
        $text_N = 'Gagal perbarui rekening.';
    }

    if ($qry) {
        $msg = 1;
    } else {
        $msg = 0;
    }
    echo "<meta http-equiv='default-style'content='0;url=?page=rekening/rek3&id=$id'> ";
}

$kode = mysql_fetch_array(mysql_query("SELECT * FROM v_rek2 WHERE idrek2 = '$id'"));
?>
<div class="container-fluid">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Tabel Rekening</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <div class="col-7" style="margin: 0 auto;">
                <?php
                if (isset($msg) && $msg !== '') {
                    if ($msg == 1) {
                        echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text_Y . '
                    </div>';
                    } elseif ($msg == 0) {
                        echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text_N . '
                    </div>';
                    }
                }
                ?>
                <form class="form-horizontal myCard" action="" method="post">
                    <input type="hidden" name="idrek2" value="<?= $kode['idrek2'] ?>">
                    <input type="hidden" name="idrek3" value="<?= $edit['idrek3'] ?>">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Kode Rekening</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="kd_rek" value="<?= $edit['koderek3'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Rekening</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_rek" value="<?= $edit['namarek3'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" id="checkboxPrimary2" name="na" <?php if (isset($edit['NA']) && $edit['NA'] == 'Y') echo 'checked' ?>>
                                    <label for="checkboxPrimary2">Tidak Aktif ?
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-success btn-sm" name="simpan"><i class="fas fa-check-circle mr-2"></i>Simpan</button>
                        <button type="reset" class="btn btn-danger btn-sm"><i class="fas fa-ban mr-2"></i>Batal</button>
                    </div>
                </form>
                Rekening : <a href="index.php?page=rekening/rek2&id=<?= $kode[idrek1] ?>"><?= $kode['koderekening2'] ?></a>
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-secondary">
                            <th width="50" class="text-center">No.</th>
                            <th width="100">Kode Rekening</th>
                            <th width="250">Rekening</th>
                            <th width="70" class="text-center">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        $sql = mysql_query("SELECT * FROM v_rek3 WHERE idrek2 = '$id'");
                        while ($data = mysql_fetch_array($sql)) {
                        ?>
                            <tr>
                                <td align="center"><?= $no ?>.</td>
                                <td align="center"><?= $data['koderekening3'] ?></td>
                                <td><?= '<a href="index.php?page=rekening/rek4&id=' . $data['idrek3'] . '">' . $data['namarek3'] . '</a>' ?></td>
                                <td align="center">
                                    <img src="./dist/img/icon/edit.png" width="22" style="cursor: pointer;" onclick="location='index.php?aks=upd&page=rekening/rek3&id=<?= $data['idrek2'] ?>&ie=<?= $data['idrek3'] ?>'" title="Perbarui data">
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>