<?php
switch ($_GET['act']) {
    case 'ins':
        $rek = "SELECT * FROM v_rekening";
?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control" name="kode_produk" id="kode_produk" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control" name="nama_produk" id="nama_produk" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Jasa Produk</label>
                                    <div class="input-group">
                                        <input type="text" name="jasa" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                        <select type="text" name="hitung" id="hitung" class="form-control">
                                            <option value="">--Pilih Hitung--</option>
                                            <?php
                                            $q_hit = mysql_query("SELECT * FROM tb_metodehitungjasa");
                                            while ($r_hit = mysql_fetch_array($q_hit)) {
                                                echo "<option value='$r_hit[id]'>$r_hit[nama]</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Jangka Waktu</label>
                                    <div class="input-group">
                                        <input type="number" name="jangka_waktu" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>Bulan</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="biaya_admin">Biaya Administrasi</label>
                                    <input type="number" class="form-control" name="biaya_admin" id="biaya_admin" placeholder="Biaya Administrasi">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_setoran">Rekening Debet Setoran</label>
                                    <select name="rekening_debet_setoran" id="rekening_debet_setoran" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_setoran">Rekening Kredit Setoran</label>
                                    <select name="rekening_kredit_setoran" id="rekening_kredit_setoran" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_penarikan">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_penarikan" id="rekening_debet_penarikan" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_jasa">Rekening Debet Jasa</label>
                                    <select name="rekening_debet_jasa" id="rekening_debet_jasa" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Jasa</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_admin">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_admin" id="rekening_debet_admin" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_admin">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_admin" id="rekening_kredit_admin" class="form-control">
                                        <option value="">--Pilih Rekening--</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script>
            $('#rekening_debet_setoran, #rekening_kredit_setoran, #rekening_debet_penarikan, #rekening_kredit_penarikan').select2();
        </script>
    <?php
        break;

    default:
    ?>
        <div class="container-fluid">
            <div class="col-12">
                <div class="card">
                    <div class="card-header ui-sortable-handle">
                        <h3 class="card-title">Daftar Produk Simpanan</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                                <i class="fas fa-minus"></i>
                            </button>
                            <a href="?page=simpananjangka&act=ins" class="btn btn-tool" title="Tambah Simpanan Modal">
                                <i class="fas fa-plus"></i>
                            </a>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No.</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Nama Produk</th>
                                    <th class="text-center">Jasa</th>
                                    <th class="text-center">Jangka Waktu</th>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $q_simo = mysql_query("SELECT * FROM tb_produksimpananjangka");
                                while ($r_simo = mysql_fetch_array($q_simo)) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $r_simo['kode_produk']; ?></td>
                                        <td><?= $r_simo['nama_produk']; ?></td>
                                        <td><?= $r_simo['jangka_waktu']; ?></td>
                                        <td class="text-right"><?= FormatRupiah($r_simo['setoran_tetap']); ?></td>
                                        <td class="text-center"><a href="?page=simpananjangka&act=edt&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        </script>
<?php
        break;
}
