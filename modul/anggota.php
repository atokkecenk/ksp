<?php
$aks = isset($_GET['aks']) ? $_GET['aks'] : '';
$ia = isset($_GET['ia']) ? $_GET['ia'] : '';
$whr = '';
if ($aks == 'upd') {
    $whr = "a.id = '$ia'";
}
$msg = '';
if (isset($_POST['simpan'])) {
    $ktp        = $_POST['ktp'];
    $nama       = $_POST['nama'];
    $tgl_lahir  = $_POST['tanggal'];
    $alamat     = $_POST['alamat'];
    $jk         = $_POST['jk'];
    $telp       = $_POST['telp'];
    $email      = $_POST['email'];
    $tgl_masuk  = $_POST['tanggal_masuk'];
    $status     = $_POST['status'];
    $email      = $_POST['email'];
    $userinput  = $_SESSION['session_user'] . '|' . date('y-m-d H:i:s');

    if ($aks !== 'upd') {
        if (cek_ktp($ktp) == 0) {
            $kode = buat_kode('tb_anggota', 'kode', 'AGT');
            $ins = mysql_query("INSERT INTO tb_anggota (kode, 
                            ktp, 
                            nama, 
                            tgl_lahir, 
                            alamat, 
                            kelamin, 
                            telp, 
                            email, 
                            tgl_masuk, 
                            status_anggota, 
                            user_input
                            ) VALUES ('$kode',
                                '$ktp',
                                '$nama',
                                '$tgl_lahir',
                                '$alamat',
                                '$jk',
                                '$telp',
                                '$email',
                                '$tgl_masuk',
                                '$status',
                                '$userinput'
                            )
                        ") or die(mysql_error());

            if ($ins) {
                $msg = 1;
                $text = 'Berhasil simpan.';
            } else {
                $msg = 0;
                $text = 'Gagal simpan data !!';
            }
        } else {
            $msg = 0;
            $text = 'No KTP sudah ada !!';
        }
        echo "<meta http-equiv='default-style'content='0;url=?page=anggota'> ";
    } else {
        $na = isset($_POST['na']) ? 'Y' : 'N';
        $upd = mysql_query("UPDATE tb_anggota SET  
                            ktp = '$ktp',
                            nama = '$nama',
                            tgl_lahir = '$tgl_lahir',
                            alamat = '$alamat',
                            kelamin = '$jk',
                            telp = '$telp',
                            email = '$email',
                            tgl_masuk = '$tgl_masuk',
                            status_anggota = '$status',
                            na = '$na'
                            WHERE id = '$ia'
                        ") or die(mysql_error());
        if ($upd) {
            $msg = 1;
            $text = 'Berhasil update data.';
        } else {
            $msg = 0;
            $text = 'Gagal update data !!';
        }
    }
}
?>
<div class="container-fluid">
    <?php
    if (isset($msg) && $msg !== '') {
        if ($msg == 1) {
            echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text . '
                    </div>';
        } elseif ($msg == 0) {
            echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ' . $text . '
                    </div>';
        }
    }
    ?>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Tambah Anggota</h3>

            <div class="card-tools">
                <div class="card-tools">
                    <a href="?page=anggota-view" class="btn btn-tool" title="Kembali ke view">
                        <i class="fas fa-arrow-left"></i> Kembali
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">No KTP</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ktp" value="<?= get_anggota('row', 'ktp', $whr) ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Nama</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="nama" value="<?= get_anggota('row', 'nama', $whr) ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Tanggal Lahir</label>
                        <div class="col-sm-7">
                            <div class="input-group date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input tanggal" name="tanggal" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="yyyy-mm-dd" value="<?= get_anggota('row', 'tgl_lahir', $whr) ?>">
                                <div class="input-group-append" data-target="#datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Alamat</label>
                        <div class="col-sm-7">
                            <textarea class="form-control" rows="3" name="alamat"><?= get_anggota('row', 'alamat', $whr) ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Jenis Kelamin</label>
                        <div class="col-sm-7" style="padding-top: 6px;">
                            <?php
                            $no = 1;
                            foreach (get_kelamin('') as $jk) {
                                $id_jk = get_anggota('row', 'kelamin', $whr);
                                $jjk = ($jk['id'] == $id_jk) ? 'checked' : null;
                                echo '
                                        <div class="icheck-primary d-inline mr-5">
                                            <input type="radio" id="jk' . $no . '" name="jk" value="' . $jk['id'] . '" ' . $jjk . '>
                                            <label for="jk' . $no . '">' . $jk['kelamin'] . '
                                            </label>
                                        </div>
                                    ';
                                $no++;
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Telepon</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="telp" value="<?= get_anggota('row', 'telp', $whr) ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Email</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="email" value="<?= get_anggota('row', 'email', $whr) ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Tanggal Masuk</label>
                        <div class="col-sm-7">
                            <div class="input-group date" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input tanggal" name="tanggal_masuk" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="yyyy-mm-dd" value="<?= get_anggota('row', 'tgl_masuk', $whr) ?>">
                                <div class="input-group-append" data-target="#datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right">Status Anggota ?</label>
                        <div class="col-sm-7" style="padding-top: 6px;">
                            <?php
                            $no = 1;
                            foreach (get_status_anggota('') as $sa) {
                                $id_sts = get_anggota('row', 'status_anggota', $whr);
                                $sts = ($sa['id'] == $id_sts) ? 'checked' : null;
                                echo '
                                        <div class="icheck-primary d-inline mr-5">
                                            <input type="radio" id="sta' . $no . '" name="status" value="' . $sa['id'] . '" ' . $sts . '>
                                            <label for="sta' . $no . '">' . $sa['status'] . '
                                            </label>
                                        </div>
                                    ';
                                $no++;
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    if (isset($_GET['aks']) == 'upd') {
                        $na = (get_anggota('row', 'na', $whr) == 'Y') ? 'checked' : null;
                    ?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-7">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" id="checkboxPrimary2" name="na" <?= $na  ?>>
                                    <label for="checkboxPrimary2">Tidak Aktif ?
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label text-right"></label>
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-success" name="simpan"><i class="fas fa-check-circle mr-2"></i>Simpan</button>
                            <button type="reset" class="btn btn-danger"><i class="fas fa-ban mr-2"></i>Batal</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

    });
</script>