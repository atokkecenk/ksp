<?php
include_once 'db.php';

$mode = $_GET['load_page'];
switch (isset($mode)) {
    case 'identitas':
        $jenis = $_GET['jenis'];
        $whr = "jenis_identitas='$jenis'";
        echo '<option>Pilih Identitas</option>';
        foreach (get_identitas('result', '', $whr) as $val) {
            echo '<option value="' . $val['id'] . '">' . $val['nama'] . '</option>';
        }
        break;
}

function buat_kode($tabel, $field, $like)
{
    $nomor = mysql_fetch_array(mysql_query("SELECT max(RIGHT($field,6)) AS no FROM $tabel WHERE $field like '$like%'"));
    $ns = $nomor['no'];
    $last_digit = $ns;
    $new_digit = '000000' . ($last_digit + 1);
    $new_nomor = $like . substr($new_digit, -6);
    return $new_nomor;
}

function get_kelamin($params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_kelamin $where") or die(mysql_error());
    $data = [];
    while ($rows = mysql_fetch_array($sql)) {
        $data[] = $rows;
    }
    return $data;
}

function get_status_anggota($params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_sts_anggota $where") or die(mysql_error());
    $data = [];
    while ($rows = mysql_fetch_array($sql)) {
        $data[] = $rows;
    }
    return $data;
}

function cek_ktp($ktp)
{
    $sql = mysql_query("SELECT * FROM tb_anggota WHERE ktp = '$ktp'") or die(mysql_error());
    if (mysql_num_rows($sql) == 0) {
        return 0;
    } else {
        return 1;
    }
}

function get_anggota($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT
                            c.`status` as sts_anggota,
                            b.icon,
                            b.kelamin as jk,
                            a.* 
                        FROM
                            `tb_anggota` a
                            LEFT JOIN tb_kelamin b ON a.kelamin = b.id
                            LEFT JOIN tb_sts_anggota c ON a.status_anggota = c.id
                            $where
                        ORDER BY
	                        a.id ASC
                            ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_jenis_identitas($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM v_jenis_identitas $where") or die(mysql_error());
    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_identitas($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM v_identitas $where") or die(mysql_error());
    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}
